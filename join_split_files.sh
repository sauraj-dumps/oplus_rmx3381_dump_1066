#!/bin/bash

cat my_preload/del-app/com.snapchat.android/com.snapchat.android.apk.* 2>/dev/null >> my_preload/del-app/com.snapchat.android/com.snapchat.android.apk
rm -f my_preload/del-app/com.snapchat.android/com.snapchat.android.apk.* 2>/dev/null
cat my_preload/del-app/com.spotify.music/com.spotify.music.apk.* 2>/dev/null >> my_preload/del-app/com.spotify.music/com.spotify.music.apk
rm -f my_preload/del-app/com.spotify.music/com.spotify.music.apk.* 2>/dev/null
cat my_preload/del-app/in.mohalla.video/in.mohalla.video.apk.* 2>/dev/null >> my_preload/del-app/in.mohalla.video/in.mohalla.video.apk
rm -f my_preload/del-app/in.mohalla.video/in.mohalla.video.apk.* 2>/dev/null
cat my_preload/del-app/realme_Link_exp/realmeLink-releaseExp-1.2.258.12_470_50937ab_202101281942_signed.apk.* 2>/dev/null >> my_preload/del-app/realme_Link_exp/realmeLink-releaseExp-1.2.258.12_470_50937ab_202101281942_signed.apk
rm -f my_preload/del-app/realme_Link_exp/realmeLink-releaseExp-1.2.258.12_470_50937ab_202101281942_signed.apk.* 2>/dev/null
cat my_preload/del-app/in.mohalla.sharechat/in.mohalla.sharechat.apk.* 2>/dev/null >> my_preload/del-app/in.mohalla.sharechat/in.mohalla.sharechat.apk
rm -f my_preload/del-app/in.mohalla.sharechat/in.mohalla.sharechat.apk.* 2>/dev/null
cat my_product/app/OplusCamera/OplusCamera.apk.* 2>/dev/null >> my_product/app/OplusCamera/OplusCamera.apk
rm -f my_product/app/OplusCamera/OplusCamera.apk.* 2>/dev/null
cat system/system/apex/com.google.android.art.apex.* 2>/dev/null >> system/system/apex/com.google.android.art.apex
rm -f system/system/apex/com.google.android.art.apex.* 2>/dev/null
cat recovery.img.* 2>/dev/null >> recovery.img
rm -f recovery.img.* 2>/dev/null
cat my_stock/priv-app/OppoGallery2/OppoGallery2.apk.* 2>/dev/null >> my_stock/priv-app/OppoGallery2/OppoGallery2.apk
rm -f my_stock/priv-app/OppoGallery2/OppoGallery2.apk.* 2>/dev/null
cat my_stock/del-app/RMrealmeLinkExp_region/realmeLink-releaseExp.apk.* 2>/dev/null >> my_stock/del-app/RMrealmeLinkExp_region/realmeLink-releaseExp.apk
rm -f my_stock/del-app/RMrealmeLinkExp_region/realmeLink-releaseExp.apk.* 2>/dev/null
cat my_bigball/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> my_bigball/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f my_bigball/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat my_bigball/app/Photos/Photos.apk.* 2>/dev/null >> my_bigball/app/Photos/Photos.apk
rm -f my_bigball/app/Photos/Photos.apk.* 2>/dev/null
cat my_bigball/app/Browser/Browser.apk.* 2>/dev/null >> my_bigball/app/Browser/Browser.apk
rm -f my_bigball/app/Browser/Browser.apk.* 2>/dev/null
cat my_bigball/priv-app/Messages/Messages.apk.* 2>/dev/null >> my_bigball/priv-app/Messages/Messages.apk
rm -f my_bigball/priv-app/Messages/Messages.apk.* 2>/dev/null
cat my_bigball/priv-app/KeKeUserCenter/KeKeUserCenter.apk.* 2>/dev/null >> my_bigball/priv-app/KeKeUserCenter/KeKeUserCenter.apk
rm -f my_bigball/priv-app/KeKeUserCenter/KeKeUserCenter.apk.* 2>/dev/null
cat my_bigball/del-app/OplusVideoEditor/OplusVideoEditor.apk.* 2>/dev/null >> my_bigball/del-app/OplusVideoEditor/OplusVideoEditor.apk
rm -f my_bigball/del-app/OplusVideoEditor/OplusVideoEditor.apk.* 2>/dev/null
cat my_bigball/del-app-pre/Duo_del/Duo.apk.* 2>/dev/null >> my_bigball/del-app-pre/Duo_del/Duo.apk
rm -f my_bigball/del-app-pre/Duo_del/Duo.apk.* 2>/dev/null
cat my_bigball/del-app-pre/Facebook/Facebook.apk.* 2>/dev/null >> my_bigball/del-app-pre/Facebook/Facebook.apk
rm -f my_bigball/del-app-pre/Facebook/Facebook.apk.* 2>/dev/null
cat odm/lib64/libstblur_capture_api.so.* 2>/dev/null >> odm/lib64/libstblur_capture_api.so
rm -f odm/lib64/libstblur_capture_api.so.* 2>/dev/null
cat system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null >> system_ext/priv-app/SystemUI/SystemUI.apk
rm -f system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat my_heytap/app/Maps/Maps.apk.* 2>/dev/null >> my_heytap/app/Maps/Maps.apk
rm -f my_heytap/app/Maps/Maps.apk.* 2>/dev/null
cat my_heytap/app/YouTube/YouTube.apk.* 2>/dev/null >> my_heytap/app/YouTube/YouTube.apk
rm -f my_heytap/app/YouTube/YouTube.apk.* 2>/dev/null
cat my_heytap/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> my_heytap/app/WebViewGoogle/WebViewGoogle.apk
rm -f my_heytap/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat my_heytap/app/Gmail2/Gmail2.apk.* 2>/dev/null >> my_heytap/app/Gmail2/Gmail2.apk
rm -f my_heytap/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat my_heytap/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> my_heytap/priv-app/GmsCore/GmsCore.apk
rm -f my_heytap/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat my_heytap/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> my_heytap/priv-app/Velvet/Velvet.apk
rm -f my_heytap/priv-app/Velvet/Velvet.apk.* 2>/dev/null
